# Documentation

## Docker Usage

```bash
$ docker build -t documentation .
$ docker run -p <desired-port>:80 documentation
```
