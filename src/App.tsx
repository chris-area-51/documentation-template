import classNames from 'classnames';
import { BrowserRouter, Route } from 'react-router-dom';

import { data } from './data/data';
import { INavigationItem } from './interfaces';
import Overlay from './components/common/Overlay';
import HeaderBar from './components/navigation/HeaderBar';
import SideNavBar from './components/navigation/SideNavBar';
import EndpointTemplates from './templates/EndpointTemplate';

const App = () => {
  const sideNavBarData: INavigationItem[] = data.map((collection) => ({
    label: collection.label,
    children: collection.children
      ? collection.children.map((child) => ({
        label: child.label,
        link: child.link,
      }))
      : undefined,
    link: collection.link ? collection.link : undefined,
  }));

  const rootClass = classNames(
    'flex',
    'w-full',
    'h-full',
    'flex-col',
  );

  const mainContentClass = classNames(
    'flex',
    'w-full',
    'flex-row',
    'xl:h-main-content',
    'h-main-content-mobile',
  );

  return (
    <BrowserRouter>
      {
        data.map((collection) => (
          <>
            {
              collection.children && collection.children.map((child, childIndex) => (
                <Route key={`subroute-${childIndex}`} path={child.link}>
                  <div className={rootClass}>
                    <Overlay />
                    <HeaderBar />
                    <div className={mainContentClass}>
                      <SideNavBar data={sideNavBarData} />
                      <EndpointTemplates
                        title={`${collection.label} - ${child.label}`}
                        data={child.collections}
                      />
                    </div>
                  </div>
                </Route>
              ))
            }
            {
              collection.link && collection.collections && (
                <Route path={collection.link}>
                  <div className={rootClass}>
                    <Overlay />
                    <HeaderBar />
                    <div className={mainContentClass}>
                      <SideNavBar data={sideNavBarData} />
                      <EndpointTemplates
                        title={`${collection.label}`}
                        data={collection.collections}
                      />
                    </div>
                  </div>
                </Route>
              )
            }
          </>
        ))
      }
    </BrowserRouter>
  );
};

export default App;
