import React from 'react';
import classNames from 'classnames';

import { ILineBreakerProps } from '../../interfaces';

const LineBreaker: React.FC<ILineBreakerProps> = (props) => {
  const { color, margin } = props;

  const lineBreakerClass = classNames(
    'border-b',
    `border-${color}`,
    'w-full',
    margin,
  );

  return (
    <div className={lineBreakerClass} />
  );
};

export default LineBreaker;
