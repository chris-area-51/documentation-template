/* eslint-disable quote-props */
import classNames from 'classnames';
import { useSelector } from 'react-redux';

import { selectMobileSideNavOpen } from '../../redux/mobileSideNavOpenSlice';

const Overlay = () => {
  const { mobileSideNavOpen } = useSelector(selectMobileSideNavOpen);

  const rootClass = classNames(
    'z-20',
    'top-0',
    'left-0',
    'absolute',
    'w-screen',
    'h-screen',
    'xl:hidden',
    'opacity-60',
    'bg-gray-700',
    {
      'block': mobileSideNavOpen,
      'hidden': !mobileSideNavOpen,
    },
  );

  return (
    <div className={rootClass} />
  );
};

export default Overlay;
