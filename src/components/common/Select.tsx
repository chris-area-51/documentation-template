/* eslint-disable no-unused-vars */
/* eslint-disable jsx-a11y/anchor-is-valid */
import React from 'react';
import classNames from 'classnames';
import { Transition } from '@headlessui/react';
import { ChevronDownIcon } from '@heroicons/react/solid';

import { ISelectProps } from '../../interfaces';

const Select: React.FC<ISelectProps> = (props) => {
  const {
    value, setValue, open, setOpen, options, buttonWidth, wrapperWidth,
  } = props;

  const rootClass = classNames(
    'relative',
    'text-left',
    'inline-block',
  );

  const openMenuClass = classNames(
    'border',
    'px-4',
    'py-2',
    'text-sm',
    'bg-white',
    'shadow-sm',
    'rounded-md',
    'inline-flex',
    'font-medium',
    'text-gray-700',
    'justify-between',
    'border-gray-300',
    'hover:bg-gray-50',
    buttonWidth,
  );

  const optionWrapperClass = classNames(
    'mt-2',
    'ring-1',
    'right-0',
    'absolute',
    'bg-white',
    'shadow-lg',
    'rounded-md',
    'ring-black',
    'ring-opacity-5',
    'origin-top-right',
    'focus:outline-none',
    wrapperWidth,
  );

  const optionClass = classNames(
    'px-4',
    'py-2',
    'block',
    'text-sm',
    'text-gray-700',
  );

  return (
    <div className={rootClass}>
      <div>
        <button
          type="button"
          id="menu-button"
          className={openMenuClass}
          onClick={() => setOpen(!open)}
        >
          { value }
          <ChevronDownIcon className="w-5 h-5" />
        </button>
      </div>
      <Transition
        show={open}
        enter="transition ease-out duration-200 transform"
        enterFrom="opacity-0 scale-95"
        enterTo="transform opacity-100 scale-100 transform"
        leave="transition ease-in duration-200 transform"
        leaveFrom="opacity-100 scale-100"
        leaveTo="transform opacity-0 scale-95"
      >
        <div
          role="menu"
          tabIndex={-1}
          className={optionWrapperClass}
        >
          <div className="py-1" role="none">
            {
              options.map((option, optionIndex) => (
                <a
                  href="#"
                  tabIndex={-1}
                  role="menuitem"
                  key={`option-${optionIndex}`}
                  className={optionClass}
                  onClick={() => { setValue(option); setOpen(false); }}
                >
                  { option }
                </a>
              ))
            }
          </div>
        </div>
      </Transition>
    </div>
  );
};

export default Select;
