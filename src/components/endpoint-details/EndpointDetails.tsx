/* eslint-disable no-shadow */
/* eslint-disable consistent-return */
import classNames from 'classnames';
import React, { useEffect } from 'react';
import { Transition } from '@headlessui/react';
import { useDispatch, useSelector } from 'react-redux';
import { Prism as SyntaxHighlighter } from 'react-syntax-highlighter';
import { tomorrow } from 'react-syntax-highlighter/dist/esm/styles/prism';

import { createSubGroup } from '../../utils';
import LineBreaker from '../common/LineBreaker';
import { IEndpointDetailsProps } from '../../interfaces';
import RequestParamsSection from './RequestParamsSection';
import { selectEndpointDetailsOpen, updateEndpointDetailsOpen } from '../../redux/endpointDetailsOpenSlice';

const EndpointDetails: React.FC<IEndpointDetailsProps> = (props) => {
  const {
    header, method, url, sections, responses,
  } = props;

  const dispatch = useDispatch();
  const { endpointDetailsOpen } = useSelector(selectEndpointDetailsOpen);

  const [resCurrentTab, setResCurrentTab] = React.useState(0);

  useEffect(() => {
    if (window.innerWidth > 1280) {
      dispatch(updateEndpointDetailsOpen({ open: true }));
    } else {
      dispatch(updateEndpointDetailsOpen({ open: false }));
    }

    window.addEventListener('resize', () => {
      if (window.innerWidth > 1280) {
        dispatch(updateEndpointDetailsOpen({ open: true }));
      } else {
        dispatch(updateEndpointDetailsOpen({ open: false }));
      }
    });
  }, []);

  const determineMethodColor = (method: string) => {
    switch (method) {
      case 'GET':
        return 'text-blue-500';
      case 'POST':
        return 'text-green-500';
      default:
        break;
    }
  };

  const rootClass = classNames(
    'flex',
    'z-10',
    'top-0',
    'h-full',
    'right-0',
    'xl:w-1/2',
    'absolute',
    'xl:relative',
    'bg-gray-800',
    'overflow-y-auto',
    'custom-scrollbar',
    'w-endpoint-details-xs',
    'md:w-endpoint-details-lg',
    'sm:w-endpoint-details-sm',
  );

  const headerClass = classNames(
    'mb-4',
    'pt-6',
    'px-8',
    'sm:pt-8',
    'sm:px-10',
    'lg:px-16',
    'lg:pt-12',
    'font-bold',
    'sm:text-xl',
    'lg:text-2xl',
    'text-gray-300',
    'tracking-tight',
  );

  const endpointClass = classNames(
    'flex',
    'mb-4',
    'px-8',
    'w-full',
    'sm:px-10',
    'flex-row',
    'lg:px-16',
    'items-center',
  );

  const methodClass = (method: string) => classNames(
    'px-4',
    'py-1',
    'bg-gray-700',
    'rounded-md',
    'font-bold',
    'text-sm',
    'md:text-base',
    determineMethodColor(method),
  );

  const requestWrapperClass = classNames(
    'px-8',
    'py-10',
    'w-full',
    'sm:px-10',
    'lg:py-12',
    'lg:px-16',
  );

  const requestClass = classNames(
    'text-base',
    'font-bold',
    'md:text-lg',
    'lg:text-xl',
    'text-gray-300',
    'tracking-tight',
  );

  const responseClass = classNames(
    'mb-4',
    'font-bold',
    'text-base',
    'md:text-lg',
    'lg:text-xl',
    'text-gray-300',
    'tracking-tight',
    {
      'mt-12': sections.length !== 0,
      'sm:mt-16': sections.length !== 0,
      'lg:mt-20': sections.length !== 0,
      'mt-4': sections.length === 0,
      'sm:mt-8': sections.length === 0,
      'lg:mt-12': sections.length === 0,
    },
  );

  const responseButtonClass = (status: string) => classNames(
    'py-1',
    'text-sm',
    'font-bold',
    'rounded-md',
    'md:text-base',
    {
      'bg-green-900': status.includes('200'),
      'text-green-300': status.includes('200'),
      'bg-red-900': !status.includes('200'),
      'text-red-300': !status.includes('200'),
    },
  );

  return (
    <Transition
      show={endpointDetailsOpen}
      enter="transition ease-in-out duration-500 transform"
      enterFrom="translate-x-full"
      enterTo="translate-x-0"
      leave="transition ease-in-out duration-500 transform"
      leaveFrom="translate-x-0"
      leaveTo="translate-x-full"
      className={rootClass}
    >
      <div className="w-full">
        <h1 className={headerClass}>{ header }</h1>
        <LineBreaker color="gray-500" margin="mb-4" />
        <div className={endpointClass}>
          <p className={methodClass(method)}>{ method }</p>
          <p className="text-sm md:text-base text-gray-400 ml-8 break-all">{ url }</p>
        </div>
        <div className={requestWrapperClass}>
          <h2 className={requestClass}>Request</h2>
          {
            sections.map((section, sectionIndex) => (
              <RequestParamsSection
                key={`section-${sectionIndex}`}
                header={section.header}
                params={section.params}
                className={sectionIndex === 0 ? 'mt-4' : 'mt-12'}
              />
            ))
          }
          {
            sections.length === 0 && (
              <h3 className="text-gray-500 mt-2">No request parameters expected.</h3>
            )
          }
          <h2 className={responseClass}>Response</h2>
          {
            responses.length && (
              <div className="flex flex-col">
                {
                  createSubGroup(responses, 4).map((resRow, resRowIndex) => (
                    <div key={`res-row-${resRowIndex}`} className="grid grid-cols-4 gap-4 w-full mb-4">
                      {
                        resRow.map((res, resIndex) => (
                          <button
                            type="button"
                            key={`res-button-${resIndex}`}
                            className={responseButtonClass(res.status)}
                            onClick={() => setResCurrentTab(resIndex)}
                          >
                            { res.status }
                          </button>
                        ))
                      }
                    </div>
                  ))
                }
              </div>
            )
          }
          {
            responses.length && (
              <SyntaxHighlighter language="json" style={tomorrow} className="h-100 overflow-y-auto">
                { JSON.stringify(responses[resCurrentTab].object, null, '\t') }
              </SyntaxHighlighter>
            )
          }
        </div>
      </div>
    </Transition>
  );
};

export default EndpointDetails;
