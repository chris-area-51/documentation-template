import React from 'react';
import classNames from 'classnames';

import LineBreaker from '../common/LineBreaker';
import { IRequestParamsSectionProps } from '../../interfaces';

const RequestParamsSection: React.FC<IRequestParamsSectionProps> = (props) => {
  const { header, params, className } = props;

  const rootClass = classNames('w-full', className);
  const headerClass = classNames(
    'mb-4',
    'text-sm',
    'sm:text-base',
    'text-gray-300',
    'tracking-tight',
  );

  const attributeClass = classNames(
    'pr-4',
    'w-1/3',
    'text-sm',
    'text-right',
    'sm:text-base',
  );

  const descriptionClass = classNames(
    'pl-4',
    'w-2/3',
    'text-sm',
    'text-left',
    'sm:text-base',
  );

  const leftColumnWrapperClass = classNames(
    'flex',
    'pr-4',
    'w-1/3',
    'text-sm',
    'flex-row',
    'sm:text-base',
  );

  const leftColumnClass = classNames('flex', 'flex-col', 'w-full', 'items-end');
  const remarkClass = classNames(
    'pl-4',
    'w-2/3',
    'text-sm',
    'text-left',
    'sm:text-base',
  );

  return (
    <div className={rootClass}>
      <h3 className={headerClass}>
        { header }
      </h3>
      <div className="text-gray-400 w-full">
        <div className="flex flex-row w-full">
          <h4 className={attributeClass}>Attribute</h4>
          <h4 className={descriptionClass}>Description</h4>
        </div>
        <LineBreaker color="gray-500" margin="my-2" />
        {
          params.map((param) => (
            <>
              <div className="flex flex-row w-full">
                <div className={leftColumnWrapperClass}>
                  <div className={leftColumnClass}>
                    <p className="break-all text-sm sm:text-base">{ param.name }</p>
                    <p className="text-gray-500">{ param.dataType }</p>
                  </div>
                </div>
                <p className={remarkClass}>{ param.remark }</p>
              </div>
              <LineBreaker color="gray-700" margin="my-2" />
            </>
          ))
        }
      </div>
    </div>
  );
};

export default RequestParamsSection;
