import React from 'react';
import classNames from 'classnames';
import { useDispatch } from 'react-redux';
import { MenuIcon } from '@heroicons/react/solid';

import Select from '../common/Select';
import { updateMobileSideNavOpen } from '../../redux/mobileSideNavOpenSlice';

const HeaderBar = () => {
  const dispatch = useDispatch();
  const [environment, setEnvironment] = React.useState('Alpha');
  const [openEnvironment, setOpenEnvironment] = React.useState(false);

  const rootClass = classNames(
    'px-9',
    'z-10',
    'h-12',
    'shadow',
    'w-full',
    'xl:h-16',
    'bg-white',
  );

  const innerClass = classNames(
    'flex',
    'h-full',
    'flex-row',
    'items-center',
    'justify-center',
    'xl:justify-between',
  );

  const menuIconClass = classNames(
    'w-5',
    'h-5',
    'ml-4',
    'block',
    'left-0',
    'absolute',
    'xl:hidden',
  );

  const selectWrapperClass = classNames(
    'hidden',
    'h-full',
    'xl:flex',
    'flex-row',
    'items-center',
  );

  return (
    <div className={rootClass}>
      <div className={innerClass}>
        <MenuIcon
          className={menuIconClass}
          onClick={() => dispatch(updateMobileSideNavOpen({ open: true }))}
        />
        <img src="/header-bar-logo.svg" alt="Hex Trust Logo" className="h-6" />
        <div className={selectWrapperClass}>
          <p className="text-sm mr-4">ENVIRONMENT</p>
          <Select
            open={openEnvironment}
            setOpen={setOpenEnvironment}
            value={environment}
            setValue={setEnvironment}
            options={['Alpha', 'Beta', 'UAT']}
            buttonWidth="w-40"
            wrapperWidth="w-56"
          />
        </div>
      </div>
    </div>
  );
};

export default HeaderBar;
