/* eslint-disable quote-props */
import classNames from 'classnames';
import React, { useEffect } from 'react';
import { Transition } from '@headlessui/react';
import { useDispatch, useSelector } from 'react-redux';
import { NavLink, useLocation } from 'react-router-dom';
import { ChevronDownIcon, XIcon } from '@heroicons/react/solid';

import { SideNavBarProps } from '../../interfaces';
import { selectMobileSideNavOpen, updateMobileSideNavOpen } from '../../redux/mobileSideNavOpenSlice';
import { selectNavItemsOpenList, updateOpen, updateInitialOpen } from '../../redux/navItemsOpenListSlice';

const SideNavBar: React.FC<SideNavBarProps> = (props) => {
  const { data } = props;
  const location = useLocation();
  const dispatch = useDispatch();
  const { navItemsOpenList } = useSelector(selectNavItemsOpenList);
  const { mobileSideNavOpen } = useSelector(selectMobileSideNavOpen);

  const updateNavItemsOpenList = (index: number) => {
    dispatch(updateOpen({ itemIndex: index }));
  };

  useEffect(() => {
    data.forEach((navigationItem, itemIndex) => {
      if (typeof navigationItem.link !== 'undefined') {
        if (location.pathname === navigationItem.link) {
          dispatch(updateInitialOpen({ itemIndex }));
        }
      }

      if (typeof navigationItem.children !== 'undefined') {
        navigationItem.children.forEach((navSubItem) => {
          if (location.pathname === navSubItem.link) {
            dispatch(updateInitialOpen({ itemIndex }));
          }
        });
      }
    });
  }, []);

  useEffect(() => {
    if (window.innerWidth > 1280) {
      dispatch(updateMobileSideNavOpen({ open: true }));
    } else {
      dispatch(updateMobileSideNavOpen({ open: false }));
    }

    window.addEventListener('resize', () => {
      if (window.innerWidth > 1280) {
        dispatch(updateMobileSideNavOpen({ open: true }));
      } else {
        dispatch(updateMobileSideNavOpen({ open: false }));
      }
    });
  }, []);

  const rootClass = classNames(
    'px-6',
    'py-6',
    'z-30',
    'top-0',
    'xl:z-0',
    'h-full',
    'absolute',
    'xl:relative',
    'bg-indigo-50',
    'overflow-y-auto',
    'custom-scrollbar',
    'xl:w-side-nav-bar',
    'w-side-nav-bar-mobile',
  );

  const firstLayerLinkClass = (isFirst: boolean) => classNames(
    'flex',
    'items-center',
    'justify-between',
    'px-3',
    'py-2',
    'text-gray-600',
    'transition-colors',
    'duration-200',
    'rounded-md',
    { 'mt-2': !isFirst },
  );

  const subMenuClass = (index: number) => classNames(
    'space-y-2',
    'px-6',
    'transition-all',
    'duration-300',
    'ease-in-out',
    {
      'opacity-0': !navItemsOpenList[index],
      'mt-0': !navItemsOpenList[index],
      'opacity-100': navItemsOpenList[index],
      'mt-1': navItemsOpenList[index],
      [`h-${(navItemsOpenList.length * 6)}`]: navItemsOpenList[index],
      'h-0': !navItemsOpenList[index],
    },
  );

  const labelClass = classNames(
    'text-sm',
    'text-gray-700',
    'font-bold',
  );

  const navChildClass = (index: number) => classNames(
    'block',
    'text-sm',
    'text-gray-600',
    'transition-colors',
    'duration-200',
    'rounded-md',
    'hover:text-blue-700',
    'hover:font-bold',
    {
      'pointer-events-none': !navItemsOpenList[index],
    },
  );

  const chevronClass = (index: number) => classNames(
    'w-5',
    'h-5',
    'transform',
    { 'rotate-180': navItemsOpenList[index] },
  );

  return (
    <Transition
      show={mobileSideNavOpen}
      enter="transition ease-in-out duration-500 transform"
      enterFrom="-translate-x-full"
      enterTo="translate-x-0"
      leave="transition ease-in-out duration-500 transform"
      leaveFrom="translate-x-0"
      leaveTo="-translate-x-full"
      className={rootClass}
    >
      <nav className="flex-1 w-full space-y-2">
        <div className="flex flex-row justify-end items-center w-full h-5 xl:hidden">
          <XIcon
            className="w-5 h-5"
            onClick={() => dispatch(updateMobileSideNavOpen({ open: false }))}
          />
        </div>
        <div>
          {
            data.map((firstLayer, firstLayerIndex) => (
              <>
                {
                  firstLayer.link && !firstLayer.children && (
                    <>
                      <NavLink to={firstLayer.link} className={firstLayerLinkClass(firstLayerIndex === 0)}>
                        <h5 className={labelClass}>{ firstLayer.label }</h5>
                      </NavLink>
                    </>
                  )
                }
                {
                  !firstLayer.link && firstLayer.children && (
                    <>
                      <NavLink
                        to="#"
                        className={firstLayerLinkClass(firstLayerIndex === 0)}
                        onClick={() => updateNavItemsOpenList(firstLayerIndex)}
                      >
                        <h5 className={labelClass}>{ firstLayer.label }</h5>
                        <ChevronDownIcon className={chevronClass(firstLayerIndex)} />
                      </NavLink>
                      <div
                        role="menu"
                        className={subMenuClass(firstLayerIndex)}
                      >
                        {
                          firstLayer.children.map((child, childIndex) => (
                            <NavLink
                              key={`nav-child-${childIndex}`}
                              to={child.link}
                              className={navChildClass(firstLayerIndex)}
                              activeClassName="text-blue-700 font-bold"
                            >
                              { child.label }
                            </NavLink>
                          ))
                        }
                      </div>
                    </>
                  )
                }
              </>
            ))
          }
        </div>
      </nav>
    </Transition>
  );
};

export default SideNavBar;
