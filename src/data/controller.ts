/* eslint-disable max-len */
export const controllerEndpoints = {
  label: 'Controller',
  link: '/controller',
  collections: [{
    header: 'Get All Wallet Addresses',
    method: 'GET',
    description: 'Get all the hexsafe tracking wallet addresses from Redis (sets with names <blockchain>.address).',
    url: '/api/v1/controller/address/all',
    sections: [],
    responses: [{
      status: '200',
      object: ['addresses'],
    }],
  }, {
    header: 'Get All Wallet Addresses By Blockchain',
    method: 'GET',
    description: 'Get all the wallet addresses of specific blockchain from Redis.',
    url: '/api/v1/controller/address/asset_ticker/{assetTicker}',
    sections: [{
      header: 'Parameter',
      params: [{
        name: 'assetTicker',
        dataType: 'string',
        remark: 'Blockchain tickers, e.g. BTC, ETH, XRP, XLM.',
      }],
    }],
    responses: [{
      status: '200',
      object: ['addresses'],
    }],
  }, {
    header: 'Get ERC20 Metadata',
    method: 'GET',
    description: 'Get details for ERC20 tokens like the smallest decimal places and naming representation.',
    url: '/api/v1/controller/get-erc20-metadata',
    sections: [],
    responses: [{
      status: '200',
      object: {
        '[erc20Name]': {
          address: 'string',
          token_name: 'string',
          token_symbol: 'string',
          token_decimal: 0,
          created_at: 'string',
          updated_at: 'string',
          created_by_user_id: 'string',
          deleted_by_user_id: 'string',
          updated_by_user_id: 'string',
          delete_flag: 0,
        },
      },
    }],
  }, {
    header: 'Get Latest Received Data for Specific Blockchain',
    method: 'GET',
    description: 'Get the block number and timestamp of the latest received data.',
    url: '/api/v1/controller/latest_received/asset_ticker/{assetTicker}',
    sections: [{
      header: 'Parameter',
      params: [{
        name: 'assetTicker',
        dataType: 'string',
        remark: 'Blockchain tickers, e.g. BTC, ETH, XRP, XLM.',
      }],
    }],
    responses: [{
      status: '200',
      object: {
        block_no: 'string',
        timestamp: 'string',
      },
    }],
  }, {
    header: 'Add New Transaction',
    method: 'POST',
    description: 'Add new transaction to the system and update the corresponding database tables.',
    url: '/api/v1/controller/new-trans',
    sections: [{
      header: 'Body',
      params: [{
        name: 'trans',
        dataType: 'object',
        remark: 'A standardized transaction object that will be received from all blockchain pipelines. Please refer to interface IControllerTx in pipeline codes for complete object structure.',
      }],
    }],
    responses: [{
      status: '200',
      object: {
        status: true,
      },
    }],
  }, {
    header: 'Add Only New Transaction',
    method: 'POST',
    description: 'New method for just flushing filtered transactions. This method is invoked by new pipeline services which themselves handle tracking and balance updates.',
    url: '/api/v1/controller/new-only-trans',
    sections: [{
      header: 'Body',
      params: [{
        name: 'trans',
        dataType: 'object',
        remark: 'A standardized transaction object that will be received from all blockchain pipelines. Please refer to interface IControllerTx in pipeline codes for complete object structure.',
      }],
    }],
    responses: [{
      status: '200',
      object: {
        status: true,
      },
    }],
  }, {
    header: 'Update Transaction',
    method: 'POST',
    description: 'Update confirmations for transactions that are being tracked and not yet completed.',
    url: '/api/v1/controller/update-trans',
    sections: [{
      header: 'Body',
      params: [{
        name: 'asset_ticker',
        dataType: 'string',
        remark: 'Blockchain tickers, e.g. BTC, ETH, XRP, XLM.',
      }, {
        name: 'txCon',
        dataType: 'object',
        remark: 'A standardized confirmation object that will be received from all blockchain pipelines. Please refer to interface IControllerConfirm in pipeline codes for complete object structure.',
      }],
    }],
    responses: [{
      status: '200',
      object: {
        status: true,
      },
    }],
  }],
};

export default controllerEndpoints;
