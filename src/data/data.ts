import { ICollection } from '../interfaces';
import { controllerEndpoints } from './controller';
import { otherEndpoints, transferEndpoints, walletEndpoints } from './zerokey';

export const data: ICollection[] = [
  controllerEndpoints,
  {
    label: 'Zero Key v2',
    children: [
      transferEndpoints,
      walletEndpoints,
      otherEndpoints,
    ],
  },
];

export default data;
