/* eslint-disable import/prefer-default-export */
export const otherEndpoints = {
  label: 'Other',
  link: '/zerokey/other',
  collections: [{
    header: 'Get Authentication Public Key',
    method: 'GET',
    description: 'Get the public key of input wallet label for authentication.',
    url: '/api/v2/zkw/authPublicKey?walletLabel={walletLabel}',
    sections: [{
      header: 'Query',
      params: [{
        name: 'walletLabel',
        dataType: 'string',
        remark: 'Name of wallet label, e.g. hex301-zero.',
      }],
    }],
    responses: [{
      status: '200',
      object: {
        success: true,
        result: {
          publicKey: 'string',
        },
      },
    }, {
      status: '400',
      object: { success: false },
    }],
  }],
};

export default otherEndpoints;
