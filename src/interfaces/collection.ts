import { IEndpointDetails } from './endpoint';

export interface IEndpoint {
  label: string,
  link: string,
  collections: IEndpointDetails[],
}

export interface ICollection {
  label: string,
  children?: IEndpoint[]
  link?: string,
  collections?: IEndpointDetails[],
}
