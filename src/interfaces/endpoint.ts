export interface IResponse {
  status: string,
  object: any,
}

export interface IRequestParams {
  name: string,
  dataType: string,
  remark: string,
}

export interface IRequestParamsSection {
  header: string,
  params: IRequestParams[],
  className?: string,
}

export interface IEndpointDetails {
  header: string,
  method: string,
  description: string,
  url: string,
  sections: IRequestParamsSection[],
  responses: IResponse[],
}
