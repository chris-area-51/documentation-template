export interface INavigationChild {
  label: string,
  link: string,
}

export interface INavigationItem {
  label: string,
  children?: INavigationChild[],
  link?: string,
}
