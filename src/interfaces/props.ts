/* eslint-disable no-unused-vars */
import { INavigationItem } from './navigation';
import { IEndpointDetails, IRequestParams, IResponse } from './endpoint';

export interface ILineBreakerProps {
  color: string,
  margin: string,
}

export interface SideNavBarProps {
  data: INavigationItem[],
}

export interface IRequestParamsSectionProps {
  header: string,
  params: IRequestParams[],
  className?: string,
}

export interface IEndpointDetailsProps {
  header: string,
  method: string,
  url: string,
  sections: IRequestParamsSectionProps[],
  responses: IResponse[],
}

export interface IEndpointTemplateProps {
  title: string,
  data: IEndpointDetails[],
}

export interface ISelectProps {
  value: any,
  setValue: (v: any | ((prevValue: any) => any)) => void,
  open: boolean,
  setOpen: (v: boolean | ((prevValue: boolean) => boolean)) => void,
  options: any[],
  buttonWidth: string,
  wrapperWidth: string,
}
