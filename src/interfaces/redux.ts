export interface INavItemsOpenListSliceState {
  navItemsOpenList: boolean[],
}

export interface INavItemsOpenListUpdateOpenAction {
  type: string,
  payload: {
    itemIndex: number,
  },
}

export interface IMobileSideNavOpenSliceState {
  mobileSideNavOpen: boolean,
}

export interface IEndpointDetailsOpenSliceState {
  endpointDetailsOpen: boolean,
}

export interface IMobileSideNavOpenUpdateOpenAction {
  type: string,
  payload: {
    open: boolean,
  },
}

export interface IEndpointDetailsOpenUpdateOpenAction {
  type: string,
  payload: {
    open: boolean,
  },
}
