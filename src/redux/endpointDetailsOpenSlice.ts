import { createSlice } from '@reduxjs/toolkit';

import { IEndpointDetailsOpenSliceState, IEndpointDetailsOpenUpdateOpenAction } from '../interfaces';

const initialState: IEndpointDetailsOpenSliceState = {
  endpointDetailsOpen: false,
};

const endpointDetailsOpenSlice = createSlice({
  name: 'endpointdetails',
  initialState,
  reducers: {
    updateOpen: (state: IEndpointDetailsOpenSliceState, action: IEndpointDetailsOpenUpdateOpenAction) => {
      const newState = state;
      const { open } = action.payload;

      newState.endpointDetailsOpen = open;
      return newState;
    },
  },
});

export const { updateOpen: updateEndpointDetailsOpen } = endpointDetailsOpenSlice.actions;

export const selectEndpointDetailsOpen = (state: any) => state.endpointDetailsOpen;

export const endpointDetailsOpenReducer = endpointDetailsOpenSlice.reducer;
