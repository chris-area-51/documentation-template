import { createSlice } from '@reduxjs/toolkit';

import { IMobileSideNavOpenSliceState, IMobileSideNavOpenUpdateOpenAction } from '../interfaces';

const initialState: IMobileSideNavOpenSliceState = {
  mobileSideNavOpen: false,
};

const mobileSideNavOpenSlice = createSlice({
  name: 'mobilesidenav',
  initialState,
  reducers: {
    updateOpen: (state: IMobileSideNavOpenSliceState, action: IMobileSideNavOpenUpdateOpenAction) => {
      const newState = state;
      const { open } = action.payload;

      newState.mobileSideNavOpen = open;
      return newState;
    },
  },
});

export const { updateOpen: updateMobileSideNavOpen } = mobileSideNavOpenSlice.actions;

export const selectMobileSideNavOpen = (state: any) => state.mobileSideNavOpen;

export const mobileSideNavOpenReducer = mobileSideNavOpenSlice.reducer;
