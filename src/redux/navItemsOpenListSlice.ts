import { createSlice } from '@reduxjs/toolkit';

import { data } from '../data/data';
import { INavItemsOpenListSliceState, INavItemsOpenListUpdateOpenAction } from '../interfaces';

const initialState: INavItemsOpenListSliceState = {
  navItemsOpenList: Array(data.length).fill(false),
};

const navItemsOpenListSlice = createSlice({
  name: 'navbar',
  initialState,
  reducers: {
    updateOpen: (state: INavItemsOpenListSliceState, action: INavItemsOpenListUpdateOpenAction) => {
      const newState = state;
      const { itemIndex: index } = action.payload;

      newState.navItemsOpenList[index] = !state.navItemsOpenList[index];
      return newState;
    },
    updateInitialOpen: (state: INavItemsOpenListSliceState, action: INavItemsOpenListUpdateOpenAction) => {
      const newState = state;
      const { itemIndex: index } = action.payload;

      newState.navItemsOpenList[index] = true;
      return newState;
    },
  },
});

export const { updateOpen, updateInitialOpen } = navItemsOpenListSlice.actions;

export const selectNavItemsOpenList = (state: any) => state.navItemsOpenList;

export const navItemsOpenListReducer = navItemsOpenListSlice.reducer;
