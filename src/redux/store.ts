import { configureStore } from '@reduxjs/toolkit';

import { navItemsOpenListReducer } from './navItemsOpenListSlice';
import { mobileSideNavOpenReducer } from './mobileSideNavOpenSlice';
import { endpointDetailsOpenReducer } from './endpointDetailsOpenSlice';

export default configureStore({
  reducer: {
    navItemsOpenList: navItemsOpenListReducer,
    mobileSideNavOpen: mobileSideNavOpenReducer,
    endpointDetailsOpen: endpointDetailsOpenReducer,
  },
});
