/* eslint-disable consistent-return */
import React from 'react';
import classNames from 'classnames';
import { useDispatch } from 'react-redux';
import { ChevronRightIcon } from '@heroicons/react/solid';

import { IEndpointTemplateProps } from '../interfaces';
import EndpointDetails from '../components/endpoint-details/EndpointDetails';
import { updateEndpointDetailsOpen } from '../redux/endpointDetailsOpenSlice';

const EndpointTemplate: React.FC<IEndpointTemplateProps> = (props) => {
  const { data, title } = props;

  const dispatch = useDispatch();
  const [currentEndpoint, setCurrentEndpoint] = React.useState(0);

  const determineMethodColor = (method: string) => {
    switch (method) {
      case 'GET':
        return 'bg-blue-500';
      case 'POST':
        return 'bg-green-500';
      default:
        break;
    }
  };

  const rootClass = classNames(
    'flex',
    'h-full',
    'w-full',
    'flex-row',
    'xl:w-main-content',
  );

  const mainContainerClass = classNames(
    'px-10',
    'py-10',
    'h-full',
    'w-full',
    'xl:w-1/2',
    'overflow-y-auto',
    'custom-scrollbar',
  );

  const endpointClass = (index: number) => classNames(
    'flex',
    'flex-row',
    'justify-between',
    'items-center',
    'w-full',
    'border-l-8',
    'border-black',
    'py-4',
    'px-6',
    { 'mt-10': index !== 0 },
    { 'bg-gray-300': index === currentEndpoint },
    { 'bg-white': index === currentEndpoint },
    { 'outline-black': index !== currentEndpoint },
  );

  const methodClass = (index: number, method: string) => classNames(
    'px-3',
    'py-1',
    'w-16',
    'max-w-16',
    'text-center',
    determineMethodColor(method),
    'font-bold',
    'cursor-pointer',
    { 'text-gray-200': index !== currentEndpoint },
    { 'text-gray-300': index === currentEndpoint },
  );

  return (
    <div className={rootClass}>
      <div className={mainContainerClass}>
        <h1 className="text-2xl mb-8 font-bold">{ title }</h1>
        {
          data.map((endpoint, endpointIndex) => (
            <>
              <div
                role="button"
                key={`endpoint-${endpointIndex}`}
                className={endpointClass(endpointIndex)}
                onClick={() => {
                  setCurrentEndpoint(endpointIndex);
                  dispatch(updateEndpointDetailsOpen({ open: true }));
                }}
              >
                <div className="flex flex-row ml-4">
                  <h5 className="text-lg tracking-tight mr-4">{ endpoint.header }</h5>
                  <p className={methodClass(endpointIndex, endpoint.method)}>
                    { endpoint.method }
                  </p>
                </div>
                <ChevronRightIcon className="w-7 h-7" />
              </div>
              <p className="mt-3 text-gray-500 w-full">{ endpoint.description }</p>
            </>
          ))
        }
      </div>
      <EndpointDetails
        header={data[currentEndpoint].header}
        method={data[currentEndpoint].method}
        url={data[currentEndpoint].url}
        sections={data[currentEndpoint].sections}
        responses={data[currentEndpoint].responses}
      />
    </div>
  );
};

export default EndpointTemplate;
