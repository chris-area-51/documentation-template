/* eslint-disable import/prefer-default-export */
export const createSubGroup = (source: any[], chunks: number) => new Array(Math.ceil(source.length / chunks))
  .fill(0)
  .map((_, i) => source.slice(i * chunks, i * chunks + chunks));
