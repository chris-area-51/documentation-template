/* eslint-disable quote-props */
module.exports = {
  purge: [],
  darkMode: 'media',
  theme: {
    extend: {
      colors: {

      },
      width: {
        'side-nav-bar': '300px',
        'fit-content': 'fit-content',
        'side-nav-bar-mobile': '250px',
        'endpoint-details-lg': '750px',
        'endpoint-details-sm': '500px',
        'endpoint-details-xs': '320px',
        'main-content': 'calc(100% - 300px)',
      },
      maxWidth: {
        '16': '4rem',
      },
      height: {
        'main-content-mobile': 'calc(100% - 3rem)',
        'main-content': 'calc(100% - 4rem)',
        'fit-content': 'fit-content',
        '100': '28rem',
        '104': '32rem',
        '108': '36rem',
      },
      outline: {
        'black': '1px solid black',
      },
    },
  },
  variants: {
    extend: {
      fontWeight: ['hover'],
    },
  },
  plugins: [],
};
